;CSC225 - Section 1 - Lab 7
;Andy Chen & Dennis Gelman
;R0 - Temp storage
;R1 - Not used
;R2 - Not used
;R3 - Temp storage
;R4 - Temp storage
;R5 - Beginning of frame 
;R6 - Top of stack pointer
;R7 - Stores return address for RET

	.ORIG	x3300

;*************** Starting charCount ****************

	;leave space for return value
	ADD	R6, R6, #-1

	;push return address
	ADD	R6, R6, #-1
	STR	R7, R6, #0

	;push dyn link (caller's frame ptr)
	ADD	R6, R6, #-1
	STR	R5, R6, #0

	;set new frame pointer
	ADD	R5, R6, #-1

	;allocate space for local variable - result
	ADD	R6, R6, #-1

	;Get str and c, store into R4, R3 
	LDR	R4, R5, #5 	;char c ASCII is stored into R4
	ADD	R3, R5, #4	;str is stored into R3
	LDR	R3, R3, #0	;address of word is stored into R3
	LDR	R3, R3, #0	;content of word is stored into R3 (*str)

;****************** if(*str == 0) **********************

	BRnp	ELSEIF		;if *str != 0, go to elseif
IF	STR	R3, R5, #0	;result = 0

	BRnzp	RTRN		;return
	
;*************** else if(*str == c) ********************

ELSEIF	NOT	R4, R4		;Find negative 2's complement of c ASCII
	ADD	R4, R4, #1	
	ADD	R4, R3, R4	
	BRnp	ELSE		;if *str != c, go to else
	
	LDR     R0, R5, #5	;push second parameter (c)
	ADD	R6, R6, #-1
	STR	R0, R6, #0

	LDR 	R0, R5, #4	;push first parameter (str+1)
	ADD	R0, R0, #1	;str+1
	ADD	R6, R6, #-1
	STR	R0, R6, #0

	LD      R0, CHAR_COUNT	;call function(recursive)
	JSRR    R0	

	LDR	R4, R6, #0	;result = 1 + charCount(char *str,char c)			
	ADD	R4, R4, #1	
	STR	R4, R5, #0		

	BRnzp	RTRN		;return

;************************* else *************************

ELSE	LDR     R0, R5, #5	;push second parameter (c)
	ADD	R6, R6, #-1
	STR	R0, R6, #0

	LDR 	R0, R5, #4	;push first parameter (str+1)
	ADD	R0, R0, #1	;str+1
	ADD	R6, R6, #-1
	STR	R0, R6, #0

	LD      R0, CHAR_COUNT	;call function(recursive)
	JSRR    R0	

	LDR	R4, R6, #0	;result = charCount(char *str,char c)				
	STR	R4, R5, #0			

	BRnzp	RTRN		;return	

;*********************** return *************************

RTRN	;copy result into return value
	LDR	R0, R5, #0
	STR	R0, R5, #3

	;pop local variables
	ADD	R6, R5, #1

	;pop dynamic link (into R5)
	LDR 	R5, R6, #0
	ADD	R6, R6, #1

	;pop return addr (into R7)
	LDR	R7, R6, #0
	ADD	R6, R6, #1

	;return control to caller
	RET

CHAR_COUNT .FILL   x3300

	.END